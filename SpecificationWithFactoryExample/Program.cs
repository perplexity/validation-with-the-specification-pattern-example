﻿using SpecificationPatternExample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationWithFactoryExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var validPerson = new Person
            {
                PersonId = Guid.NewGuid(),
                FirstName = "Steve",
                LastName = "Jones",
                TaxId = "888888888",
                BirthDate = DateTime.Parse("1/1/75")
            };

            SpecificationPatternExample.Helper.PrintValidation(validPerson);

            var invalidPerson = new Person
            {
                PersonId = null,
                FirstName = string.Empty,
                LastName = " ",
                TaxId = "123",
                BirthDate = DateTime.Now.AddYears(1)
            };

            SpecificationPatternExample.Helper.PrintValidation(invalidPerson);

            Console.ReadKey();

        }
    }
}
