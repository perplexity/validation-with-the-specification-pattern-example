﻿using SpecificationPatternExample.Factories;
using SpecificationPatternExample.Specifications;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SpecificationPatternExample.Validators
{
    public class PersonValidator : IValidator<Person>
    {
        private readonly List<ISpecification<Person>> _rules = new List<ISpecification<Person>>();

        public bool IsValid(Person entity)
        {
            return !BrokenRules(entity).Any();
        }

        public IEnumerable<SpecificationFeedback> BrokenRules(Person entity)
        {
            return _rules.Where(rule => !rule.IsSatisfiedBy(entity))
                .Select(rule => rule.Feedback);
        }

        public class PersonValidatorFactory : ValidatorFactoryBase<Person>
        {
            private readonly IValidationRepository _validationRepository;

            public PersonValidatorFactory(IValidationRepository validationRepository)
            {
                this._validationRepository = validationRepository;
            }

            public override IValidator<Person> Create()
            {
                var validator = new PersonValidator();
                var db = this._validationRepository;
                var a = db.FactorySpecifications
                    .Where(p => p.Entity == "Person" && p.IsActive)
                    .AsEnumerable()
                    .Select(p => new
                    {
                        record = p,
                        specification =
                            Assembly.GetExecutingAssembly().CreateInstance(p.Factory) as ISpecificationFactory<Person>,
                    })
                    .Where(p => p.specification != null)
                    .Select(p => p.specification.CreateSpecification(p.record.Feedback));
                validator._rules.AddRange(a);
                return validator;
            }
        }
    }
}
