﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpecificationPatternExample.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SpecificationPatternExample.Tests
{
    [TestClass]
    public class BruteForceTests
    {
        private static readonly BruteForcePersonValidator Validator = new BruteForcePersonValidator();

        [TestMethod]
        public void ValidPersonNoErrors()
        {
            var validPerson = new Person
            {
                PersonId = Guid.NewGuid(),
                FirstName = "Steve",
                LastName = "Smith",
                TaxId = "999999999",
                BirthDate = DateTime.Parse("1/1/75")
            };
            var rulesBroken = Validator.BrokenRules(validPerson).Any();
            Assert.IsFalse(Validator.BrokenRules(validPerson).Any());
        }

        [TestMethod]
        public void PersonInvalidTaxId()
        {
            var validPerson = new Person
            {
                PersonId = Guid.NewGuid(),
                FirstName = "Steve",
                LastName = "Smith",
                TaxId = "123",
                BirthDate = DateTime.Parse("1/1/75")
            };
            Assert.AreEqual(1, Validator.BrokenRules(validPerson).Count());
        }
    }
}
