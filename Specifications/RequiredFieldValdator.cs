﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Specifications
{
    public class RequiredFieldValdator<TEntity, TProperty> : ISpecification<TEntity>
    {
        private static Func<TEntity, TProperty> GetPropertyValueFunc = null;
        private static readonly Func<TProperty, TProperty, bool> NullableFunc = null;
        private string propName = string.Empty;
        static RequiredFieldValdator()
        {
            // create the func to be used for nullable type comparisons, and do it here so we only do it once
            // if the type isn't Nullable<T>, we don't need to do anything here
            var propType = typeof(TProperty);
            var underlyingType = Nullable.GetUnderlyingType(propType);
            if (underlyingType == null)
                return;

            /* we need an array of Param expressions for the property type a couple times, do it here for clarity and 
             * later brevity. */
            var paramExpressions = new ParameterExpression[] { Expression.Parameter(typeof(TProperty), "n1"), Expression.Parameter(typeof(TProperty), "n2") };
            /* Create a method Call expression for Nullable.Equalls for the property generic type arg; compile it to a 
             * lambda for use by calls to RequiredFieldValdator. */
            Expression callExpression = null;
            callExpression = Expression.Call(typeof(Nullable), "Equals", new Type[] { underlyingType }, (Expression[])paramExpressions);
            var rawLambda = Expression.Lambda<Func<TProperty, TProperty, bool>>(callExpression, paramExpressions);
            NullableFunc = rawLambda.Compile();
        }

        public RequiredFieldValdator(Expression<Func<TEntity, TProperty>> propertyFunc, SpecificationFeedback feedback)
        {
            /* compile a lambda for getting the actual property value; we also need a couple pieces of info about the 
             * property for later as well. */
            var lambda = propertyFunc as LambdaExpression;
            GetPropertyValueFunc = (Func<TEntity, TProperty>)lambda.Compile();
            var memberExpression = lambda.Body as MemberExpression;
            var propInfo = memberExpression.Member as PropertyInfo;
            var propType = propInfo.PropertyType;
            propName = propInfo.Name;
            Feedback = feedback;
        }

        public SpecificationFeedback Feedback { get; set; }

        public bool IsSatisfiedBy(TEntity entity)
        {
            var propertyValue = GetPropertyValueFunc(entity);

            // Is it a string
            if (typeof(TProperty) == typeof(string))
                /* Do a simple null/whitespace check. Is this a valid comparison? Does it need to be config'able to 
                 * allow Whitespace? */
                return !string.IsNullOrWhiteSpace(propertyValue as string);
            /* Is it a Nullable type; compare to the default value of TProperty to work around some semantic issues, we 
             * are of course making the assumption that the default value of Nullable<TProperty> is null. */
            else if (Nullable.GetUnderlyingType(typeof(TProperty)) != null)
                return !NullableFunc(propertyValue, default(TProperty));
            // it's nothing we can do anything with, we shouldn't hit this, throw an exception?
            else
                throw new ArgumentException("RequiredFieldValidator validating invalid type. (Not string or not nullable data type).");
        }
    }
}
