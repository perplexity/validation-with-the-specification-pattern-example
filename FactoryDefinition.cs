﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample
{
    public class FactoryDefinition
    {
        public string FactoryName { get; set; }

        public SpecificationFeedback Feedback { get; set; }

    }
}
