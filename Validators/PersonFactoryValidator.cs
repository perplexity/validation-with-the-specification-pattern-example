﻿using SpecificationPatternExample.Factories;
using SpecificationPatternExample.Specifications;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SpecificationPatternExample.Validators
{
    public class PersonFactoryValidator : IValidator<Person>
    {
        public PersonFactoryValidator(List<FactoryDefinition> factoryDefinitions)
        {
            _rules = LoadFactory(factoryDefinitions);
        }

        private IList<ISpecification<Person>> _rules;

        public bool IsValid(Person entity)
        {
            return !BrokenRules(entity).Any();
        }

        public IEnumerable<SpecificationFeedback> BrokenRules(Person entity)
        {
            return _rules.Where(rule => !rule.IsSatisfiedBy(entity))
                .Select(rule => rule.Feedback);
        }

        public List<ISpecification<Person>> LoadFactory(List<FactoryDefinition> factoryDefinitions)
        {
            var specifications = new List<ISpecification<Person>>();

            foreach (var factory in factoryDefinitions)
            {
                var tempFactory = Assembly.GetExecutingAssembly().CreateInstance(factory.FactoryName) as ISpecificationFactory<Person>;
                if (tempFactory != null) specifications.Add(tempFactory.CreateSpecification(factory.Feedback));
            }

            return specifications;
        }
    }


}
