﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SpecificationPatternExample
{
    public interface IValidationRepository : IDisposable
    {
        IQueryable<BruteForceValidationSpecificationDbEntity> BruteForceSpecifications { get; }
        IQueryable<FactoryBasedValidationSpecificationDbEntity> FactorySpecifications { get; }
    }

    public class dBProxy : IValidationRepository
    {
        private static readonly List<BruteForceValidationSpecificationDbEntity> _bruteForceSpecifications = new List<BruteForceValidationSpecificationDbEntity>()
        {
            new BruteForceValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Field = "BirthdateNotFuture", Feedback = new SpecificationFeedback(SpecificationSeverity.Warning, "Birthdate should not be in the future.")},
            new BruteForceValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Field = "BirthdateRequiredFactory", Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Birthdate is Required.")},
            new BruteForceValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Field = "LastNameNotEmpty", Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Last Name must not be blank.")},
            new BruteForceValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Field = "TaxId9Characters", Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Tax Id must be nine (9) characters.")},
            new BruteForceValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Field = "FirstNameRequired", Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "First Name is Required.")},
        };

        private static readonly List<FactoryBasedValidationSpecificationDbEntity> _factorySpecifications = new List<FactoryBasedValidationSpecificationDbEntity>()
        {
            new FactoryBasedValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Factory = "SpecificationPatternExample.Factories.BirthdateNotFutureFactory", Feedback = new SpecificationFeedback(SpecificationSeverity.Warning, "Birthdate should not be in the future.")},
            new FactoryBasedValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Factory = "SpecificationPatternExample.Factories.BirthdateRequiredFactory", Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Birthdate is Required.")},
            new FactoryBasedValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Factory = "SpecificationPatternExample.Factories.LastNameNotEmptyFactory", Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Last Name must not be blank.")},
            new FactoryBasedValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Factory = "SpecificationPatternExample.Factories.TaxId9Characters", Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Tax Id must be nine (9) characters.")},
            new FactoryBasedValidationSpecificationDbEntity(){ Entity = "Person", IsActive = true, Factory = "SpecificationPatternExample.Factories.FirstNameRequiredFactory", Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "First Name is Required.")},
        };

        public IQueryable<BruteForceValidationSpecificationDbEntity> BruteForceSpecifications { get { return _bruteForceSpecifications.AsQueryable(); } }
        public IQueryable<FactoryBasedValidationSpecificationDbEntity> FactorySpecifications { get { return _factorySpecifications.AsQueryable(); } }

        public void Dispose() { }
    }
}
