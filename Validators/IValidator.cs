﻿using System.Collections.Generic;

namespace SpecificationPatternExample.Validators
{
    public interface IValidator<T>
    {
        bool IsValid(T entity);
        IEnumerable<SpecificationFeedback> BrokenRules(T entity);
    }
}
