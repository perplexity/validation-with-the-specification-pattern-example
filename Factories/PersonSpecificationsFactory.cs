﻿using SpecificationPatternExample.Specifications;
using System;

namespace SpecificationPatternExample.Factories
{
    public class BirthdateNotFutureFactory : SpecificationFactoryBase<Person>
    {
        public override ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new BirthdateNotFuture<Person>(feedback);
        }
    }

    public class LastNameNotEmptyFactory : SpecificationFactoryBase<Person>
    {
        public override ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new LastNameNotEmpty(feedback);
        }
    }

    public class FirstNameRequiredFactory : SpecificationFactoryBase<Person>
    {
        public override ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new RequiredFieldValdator<Person, string>(p => p.FirstName, feedback);
        }
    }

    public class BirthdateRequiredFactory : SpecificationFactoryBase<Person>
    {
        public override ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new RequiredFieldValdator<Person, DateTime?>(p => p.BirthDate, feedback);
        }
    }

    public class TaxId9Characters : SpecificationFactoryBase<Person>
    {
        public override ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new TaxId9Characters<Person>(feedback);
        }
    }
}
