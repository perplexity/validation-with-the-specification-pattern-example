﻿
namespace SpecificationPatternExample
{
    public class BruteForceValidationSpecificationDbEntity
    {
        public string Entity { get; set; }
        public string Field { get; set; }
        public bool IsActive { get; set; }
        public SpecificationFeedback Feedback { get; set; }
    }

    public class FactoryBasedValidationSpecificationDbEntity
    {
        public string Entity { get; set; }
        public string Factory { get; set; }
        public bool IsActive { get; set; }
        public SpecificationFeedback Feedback { get; set; }
    }
}
