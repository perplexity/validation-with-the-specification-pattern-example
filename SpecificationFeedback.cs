﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample
{
    public class SpecificationFeedback
    {
        public SpecificationFeedback(SpecificationSeverity severity, string feedbackMessage)
        {
            Severity = severity;
            Message = feedbackMessage;
        }

        public SpecificationSeverity Severity { get; set; }
        public string Message { get; set; }
    }
}
