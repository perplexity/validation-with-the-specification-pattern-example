﻿
namespace SpecificationPatternExample.Validators
{
    public abstract class ValidatorFactoryBase<T>
    {
        public abstract IValidator<T> Create();
    }
}
