﻿using SpecificationPatternExample.Specifications;

namespace SpecificationPatternExample.Factories
{
    public interface ISpecificationFactory<T>
    {
        ISpecification<T> CreateSpecification(SpecificationFeedback feedback);
    }

    public abstract class SpecificationFactoryBase<T> : ISpecificationFactory<T>
    {
        public abstract ISpecification<T> CreateSpecification(SpecificationFeedback feedback);
    }
}
