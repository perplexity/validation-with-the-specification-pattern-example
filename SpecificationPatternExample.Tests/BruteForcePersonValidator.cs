﻿using SpecificationPatternExample.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using SpecificationPatternExample.Validators;

namespace SpecificationPatternExample.Tests
{
    public class BruteForcePersonValidator : IValidator<Person>
    {
        private static readonly IList<ISpecification<Person>> Rules = new List<ISpecification<Person>>();

        static BruteForcePersonValidator()
        {
            var db = new dBProxy();
            var specs = db.BruteForceSpecifications.Where(p => p.Entity == "Person" && p.IsActive).ToArray();

            if (specs.Any(p => p.Field.Equals("BirthdateNotFuture")))
                Rules.Add(new BirthdateNotFuture<Person>(new SpecificationFeedback(SpecificationSeverity.Error, "Birthdate must not be in the future.")));
            if (specs.Any(p => p.Field.Equals("LastNameNotEmpty")))
                Rules.Add(new LastNameNotEmpty(new SpecificationFeedback(SpecificationSeverity.Error, "Last Name must not be empty.")));
            if (specs.Any(p => p.Field.Equals("TaxId9Characters")))
                Rules.Add(new TaxId9Characters<Person>(new SpecificationFeedback(SpecificationSeverity.Error, "Tax Id must be nine characters.")));
            if (specs.Any(p => p.Field.Equals("FirstNameRequired")))
                Rules.Add(new RequiredFieldValdator<Person, string>(p => p.FirstName, new SpecificationFeedback(SpecificationSeverity.Error, "First Name is Required.")));
            if (specs.Any(p => p.Field.Equals("LastNameRequired")))
                Rules.Add(new RequiredFieldValdator<Person, DateTime?>(p => p.BirthDate, new SpecificationFeedback(SpecificationSeverity.Error, "Birth Date is Required.")));
        }

        public bool IsValid(Person entity)
        {
            return !BrokenRules(entity).Any();
        }

        public IEnumerable<SpecificationFeedback> BrokenRules(Person entity)
        {
            return Rules.Where(rule => !rule.IsSatisfiedBy(entity))
                .Select(rule => rule.Feedback);
        }
    }
}
